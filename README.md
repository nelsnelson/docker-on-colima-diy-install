# docker

Install docker from source.

## Dependencies


```sh
url=https://ftp.gnu.org/gnu/gettext/gettext-0.21.tar.gz
curl --location --remote-name $url
tar -zxf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://downloads.sourceforge.net/project/pcre/pcre/8.45/pcre-8.45.tar.bz2
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
url=https://raw.githubusercontent.com/Homebrew/formula-patches/03cf8088210822aa2c1ab544ed58ea04c897d9c4/libtool/configure-big_sur.diff
curl --location --remote-name $url
patch -p0 ./configure < $(basename $url)
./configure --enable-utf8 --enable-pcre8 --enable-pcre16 --enable-pcre32 --enable-unicode-properties --enable-pcregrep-libz --enable-pcregrep-libbz2
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://download.gnome.org/sources/glib/2.73/glib-2.73.3.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
meson setup build --default-library=both -Diconv=auto -Dbsymbolic_functions=false -Ddtrace=false
meson compile -C build --verbose
sudo meson install -C build
rm -rf build
cd ..

url=https://pkgconfig.freedesktop.org/releases/pkg-config-0.29.2.tar.gz
curl --location --remote-name $url
tar -zxf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure --with-internal-glib
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://www.gnupg.org/ftp/gcrypt/gnutls/v3.7/gnutls-3.7.7.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure --without-p11-kit
make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://www.ijg.org/files/jpegsrc.v9e.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd jpeg-9e
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://downloads.sourceforge.net/project/libpng/libpng16/1.6.38/libpng-1.6.38.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://gitlab.freedesktop.org/slirp/libslirp/-/archive/v4.7.0/libslirp-v4.7.0.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
meson ./build -Ddefault_library=both
ninja -C ./build
sudo ninja -C ./build install
rm -rf ./build
cd ..

url=https://www.openssl.org/source/openssl-1.1.1q.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
perl ./Configure no-ssl3 no-ssl3-method no-zlib darwin64-arm64-cc enable-ec_nistp_64_gcc_128
sed --in-place --expression='s/^\#include <stdio.h>/\#include <stdio.h>\n\#include <string.h>/' test/v3ext.c
time make -j$(sysctl -n hw.ncpu)
sudo make install MANSUFFIX=ssl
make test
make clean
cd ..

url=https://cmocka.org/files/1.1/cmocka-1.1.5.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
mkdir -p build
cd build
cmake .. -DWITH_STATIC_LIB=ON -DWITH_CMOCKERY_SUPPORT=ON -DUNIT_TESTING=ON -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..
rm -rf ./build
cd ..

url=https://www.libssh.org/files/0.9/libssh-0.9.6.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
mkdir -p build
cd build
# cmake -DUNIT_TESTING=ON -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug ..
cmake .. -DBUILD_STATIC_LIB=ON -DWITH_SYMBOL_VERSIONING=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_VERBOSE_MAKEFILE=ON -Wno-dev -DCMAKE_OSX_SYSROOT=$(xcrun --show-sdk-path)
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..
rm -rf ./build
cd ..

url=https://github.com/libusb/libusb/releases/download/v1.0.26/libusb-1.0.26.tar.bz2
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://www.oberhumer.com/opensource/lzo/download/lzo-2.10.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
make check
sudo make install
make clean
cd ..

url=https://ftp.gnu.org/gnu/ncurses/ncurses-6.3.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure --enable-pc-files --with-pkg-config-libdir=/usr/local/lib/pkgconfig --enable-sigwinch --enable-symlinks --enable-widec --with-shared --with-cxx-shared --with-gpm=yes --without-ada
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://ftp.gnu.org/gnu/nettle/nettle-3.8.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://cairographics.org/releases/pixman-0.40.0.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://github.com/google/snappy/archive/1.1.9.tar.gz
curl --location --remote-name $url
mv $(basename $url) snappy-$(basename $url)
tar -xf snappy-$(basename $url)
cd snappy-${$(basename "${$(basename $url)%.*}")%.*}
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_VERBOSE_MAKEFILE=ON -Wno-dev -DCMAKE_OSX_SYSROOT=$(xcrun --show-sdk-path) -DSNAPPY_BUILD_TESTS=OFF -DSNAPPY_BUILD_BENCHMARKS=OFF
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..
rm -rf ./build
cd ..

url=https://downloads.sourceforge.net/project/lzmautils/xz-5.2.5.tar.gz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://ftp.gnu.org/gnu/help2man/help2man-1.49.2.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://ftp.gnu.org/gnu/libtool/libtool-2.4.7.tar.xz
curl --silent --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
./configure
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..
pushd ~/.local/bin
ln -s /usr/local/bin/libtoolize glibtoolize
ln -s /usr/local/bin/libtool glibtool
popd

url=https://github.com/virtualsquare/vde-2/archive/refs/tags/v2.3.3.tar.gz
curl --location --remote-name $url
mv $(basename $url) vde-2-"${$(basename $url):1}"
tar -xf vde-2-"${$(basename $url):1}"
cd vde-2-${$(basename "${${$(basename $url):1}%.*}")%.*}
autoreconf --install
./configure --disable-tuntap --disable-python
time make -j$(sysctl -n hw.ncpu)
sudo make install
make clean
# make extraclean
cd ..

url=https://github.com/lima-vm/vde_vmnet/archive/refs/tags/v0.6.0.tar.gz
curl --location --remote-name $url
mv $(basename $url) vde_vmnet-"${$(basename $url):1}"
tar -xf vde_vmnet-"${$(basename $url):1}"
cd vde_vmnet-${$(basename "${${$(basename $url):1}%.*}")%.*}
time make -j$(sysctl -n hw.ncpu) PREFIX=/usr/local
sudo make PREFIX=/usr/local install.bin install.launchd
make clean
cd ..

url=https://github.com/facebook/zstd/archive/v1.5.2.tar.gz
curl --location --remote-name $url
mv $(basename $url) zstd-"${$(basename $url):1}"
tar -xf zstd-"${$(basename $url):1}"
cd zstd-${$(basename "${${$(basename $url):1}%.*}")%.*}
cmake -S build/cmake -B builddir -DZSTD_PROGRAMS_LINK_SHARED=ON -DZSTD_BUILD_CONTRIB=ON -DZSTD_LEGACY_SUPPORT=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_VERBOSE_MAKEFILE=ON -Wno-dev -DCMAKE_OSX_SYSROOT=$(xcrun --show-sdk-path)
cmake --build builddir
sudo cmake --install builddir
cmake --build builddir --target clean
cd ..

url=https://download.qemu.org/qemu-7.0.0.tar.xz
curl --location --remote-name $url
tar -xf $(basename $url)
cd ${$(basename "${$(basename $url)%.*}")%.*}
export LIBTOOL=glibtool
./configure --enable-vde --disable-bsd-user --disable-guest-agent --enable-curses --enable-libssh --enable-slirp=system --enable-vde --enable-virtfs --enable-zstd --extra-cflags=-DNCURSES_WIDECHAR=1 --disable-sdl --disable-linux-user --disable-gtk --enable-cocoa
time make V=1 -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

url=https://github.com/lima-vm/lima/archive/v0.11.1.tar.gz
curl --location --remote-name $url
mv $(basename $url) lima-"${$(basename $url):1}"
tar -xf lima-"${$(basename $url):1}"
cd lima-${$(basename "${${$(basename $url):1}%.*}")%.*}
make clean binaries -j$(sysctl -n hw.ncpu)
sudo make install
make clean
cd ..

git clone https://github.com/cpuguy83/go-md2man.git
cd go-md2man
make
cat << EOF | tee -a ./Makefile
install:
$(printf '\t')install bin/go-md2man /usr/local/bin/
$(printf '\t')install bin/go-md2man /usr/local/bin/md2man

uninstall:
$(printf '\t')rm -rf /usr/local/bin/go-md2man
$(printf '\t')rm -rf /usr/local/bin/md2man

EOF
sudo make install
cd ..

git clone https://github.com/docker/cli.git docker-cli
cd docker-cli
asdf plugin add golang
echo "golang 1.18.6" > .tool-versions
asdf install
export GOROOT=$(dirname $(dirname $(asdf which go)))
go install golang.org/x/tools/cmd/godoc@latest
export GOPATH=$(pwd)/docker_build
export PATH="${PATH}:${GOPATH}/bin"
export GO111MODULE='auto'
mkdir -p "${GOPATH}/github.com/docker/cli/cmd/docker"
go get -d github.com/docker/cli
scripts/build/binary
sudo install build/docker-darwin-arm64 /usr/local/bin/
pushd /usr/local/bin
ln -sf docker-darwin-arm64 docker
popd
cd ..

url=https://github.com/docker/buildx/releases/download/v0.9.1/buildx-v0.9.1.darwin-arm64
curl --location --remote-name $url
sudo install $(basename $url) /usr/local/bin/buildx
mkdir -p $HOME/.docker/cli-plugins
install $(basename $url) $HOME/.docker/cli-plugins/docker-buildx
docker buildx install
cd ..

# OR, build and install from source:
url=https://github.com/docker/buildx/archive/refs/tags/v0.9.1.tar.gz
curl --location --remote-name $url
mv $(basename $url) buildx-"${$(basename $url):1}"
tar -xf buildx-"${$(basename $url):1}"
cd buildx-${$(basename "${${$(basename $url):1}%.*}")%.*}
url=https://github.com/docker/buildx/releases/download/v0.9.1/buildx-v0.9.1.darwin-arm64
curl --location --remote-name $url
sudo install $(basename $url) /usr/local/bin/buildx
cd ..

url=https://github.com/docker/compose/archive/v2.11.0.tar.gz
curl --location --remote-name $url
mv $(basename $url) compose-"${$(basename $url):1}"
tar -xf compose-"${$(basename $url):1}"
cd compose-${$(basename "${${$(basename $url):1}%.*}")%.*}
export CGO_ENABLED=0
go build -trimpath -o=bin/docker-compose -ldflags="-s -w -X github.com/docker/compose/v2/internal.Version=2.11.0" ./cmd
sudo install bin/docker-compose /usr/local/bin
cd ..

git clone https://github.com/abiosoft/colima.git
cd colima
version=v0.4.4
git checkout $version
export GOROOT=$(dirname $(dirname $(which go)))
export GOPATH=
export GO111MODULE=
go build -ldflags "-X colima/config.appVersion=${version} -X colima/config.revision=8bb1101a861a8b6d2ef6e16aca97a835f65c4f8f" -o _output/binaries/colima-Darwin-arm64 ./cmd/colima
scripts/build_vmnet.sh
sudo make install
make clean
cd ..

colima start
```
